import hexRgb from 'hex-rgb';
import curve from 'cardinal-spline-js/curve';

const sOptions = Symbol('options');
const sDataset = Symbol('dataset');
const sCtx = Symbol('dataset');
const sWidth = Symbol('width');
const sHeight = Symbol('height');
const sValues = Symbol('values');
const sColumnsRanges = Symbol('columnsRanges');
const sActiveValue = Symbol('activeValue');
const sPrevDataset = Symbol('prevDataset');
const sRatio = Symbol('ratio');

export const Orientation = {
    UP: 1,
    DOWN: 2
};

const defaultOptions = {
    orientation: Orientation.UP,
    dotsColor: '#285168',
    dotsGap: 9,
    pointsGap: 30,
    marginTop: 70,
    marginBottom: 40,
    labelsMarginBottom: 12,
    pointBorder: 6,
    pointDia: 2,
    lineGradient: [
        [0, '#4f9fdd'],
        [0.2, '#40d3dd'],
        [0.4, '#49ead9'],
        [0.6, '#aaf7bf'],
        [0.8, '#d3f9b4'],
        [1, '#e8fbae'],
    ],
    activeLabelGradient: [
        [0, '#46e9db', 0.15],
        [1, '#46e9db', 0.02],
    ],
    labelLineGradient: [
        [0, '#2c9da0', 1],
        [0.6, '#2c9da0', 1],
        [1, '#2c9da0', 0],
    ],
    activeLabelBorder: '#46e9db',
    onClick: null
};

export default class Chart {
    constructor(element, options, dataset) {
        let context = element.getContext('2d'),
            devicePixelRatio = window.devicePixelRatio || 1,
            backingStoreRatio = context.webkitBackingStorePixelRatio || context.mozBackingStorePixelRatio || context.msBackingStorePixelRatio || context.oBackingStorePixelRatio || context.backingStorePixelRatio || 1,
            ratio = devicePixelRatio / backingStoreRatio;

        var oldWidth = element.offsetWidth;
        var oldHeight = element.offsetHeight;
        element.width = oldWidth * ratio;
        element.height = oldHeight * ratio;
        element.style.width = oldWidth;
        element.style.height = oldHeight;
        context.scale(ratio, ratio);

        this[sOptions] = Object.assign({}, defaultOptions, options);
        this[sDataset] = dataset;
        this[sCtx] = context;
        this[sWidth] = oldWidth;
        this[sHeight] = oldHeight;
        this[sCtx] = element.getContext('2d');
        this[sWidth] = element.offsetWidth;
        this[sHeight] = element.offsetHeight;
        this[sColumnsRanges] = [];
        this[sValues] = this.prepareValues(dataset.values);
        this[sActiveValue] = this[sValues].length - 2;
        this[sRatio] = ratio;

        element.addEventListener('click', this.clickHandler.bind(this));
        this.draw(true);
    }

    get ratio() {
        return this[sRatio];
    }

    setDataset(value, force = true) {
        this[sPrevDataset] = this[sValues];
        let newValues = this.prepareValues(value.values),
            newLabels = value.labels,
            datasetValues = value.values,
            animationStep = 5,
            completedValues = [],
            last = false;

        if (newValues.length == this[sDataset].values.length) force = false;

        if (force) {
            this[sValues] = newValues;
            this[sActiveValue] = this[sValues].length - 2;
            this[sDataset].values = datasetValues;
            this[sDataset].labels = newLabels;
            this.draw(true);
        } else {
            clearInterval(this.interval);
            this.interval = setInterval(() => {
                this[sPrevDataset].forEach((value, index) => {
                    if (value > newValues[index]) {
                        value -= animationStep;

                        if (value < newValues[index]) {
                            value = newValues[index];
                        }
                    }

                    if (value < newValues[index]) {
                        value += animationStep;

                        if (value > newValues[index]) {
                            value = newValues[index];
                        }
                    }

                    if (value === newValues[index]) {
                        if (completedValues.indexOf(index) == -1) {
                            completedValues.push(index);
                        }

                        if (completedValues.length === newValues.length) {
                            clearInterval(this.interval);
                            last = true;
                        }
                    }
                    this[sPrevDataset][index] = value;
                });

                this[sValues] = this[sPrevDataset];
                this[sDataset].values = datasetValues;
                this[sDataset].labels = newLabels;
                this.draw(last);
            }, 30);
        }
    }

    set activeValue(value) {
        this[sActiveValue] = value;
    }

    get ctx() {
        return this[sCtx];
    }

    get dataset() {
        return this[sDataset];
    }

    get options() {
        return this[sOptions];
    }

    get values() {
        return this[sValues].slice(1);
    }

    get width() {
        return this[sWidth];
    }

    get height() {
        return this[sHeight];
    }

    get activeValue() {
        return this[sActiveValue];
    }

    get heightMargins() {
        return this.options.marginTop + this.options.marginBottom;
    }

    prepareValues(datasetValues) {
        let min = Math.min(...datasetValues),
            max = Math.max(...datasetValues) - min;

        let values = datasetValues.map((item) => {
            return item - min;
        });

        min = 0;

        return values.map((item) => {
            if (item == 0) return 0;
            return 100 / (max / item);
        });
    }

    drawDots() {
        let gap = this.options.dotsGap;
        this.ctx.fillStyle = Chart.hexRgb(this.options.dotsColor);

        for (let x = gap; x < this.width; x += gap) {
            for (let y = gap; y < this.height; y += gap) {
                this.ctx.fillRect(x, y, 1, 1);
            }
        }
    }

    get columnRanges() {
        let gap = this.options.pointsGap,
            labels = this.dataset.labels,
            columnWidth = this.columnWidth,
            columnRanges = [];

        for (let x = columnWidth / 2, labelIdx = 0; x < this.width - gap, labelIdx < labels.length; x += columnWidth, ++labelIdx) {
            columnRanges.push([x - columnWidth / 2, x + columnWidth / 2]);
        }

        return columnRanges;
    }

    drawLabels() {
        let gap = this.options.pointsGap,
            labels = this.dataset.labels,
            columnWidth = this.columnWidth;

        this.ctx.beginPath();
        this.ctx.moveTo(0, this.height - 1);
        this.ctx.lineTo(this.width, this.height - 1);
        this.ctx.lineWidth = 2;
        this.ctx.strokeStyle = 'rgba(255, 255, 255, 0.3)';
        this.ctx.stroke();
        this.ctx.closePath();

        this.ctx.beginPath();
        this.ctx.moveTo(this.activeValue * columnWidth, this.height - 1);
        this.ctx.lineTo(this.activeValue * columnWidth + columnWidth, this.height - 1);
        this.ctx.strokeStyle = this.options.activeLabelBorder;
        this.ctx.stroke();
        this.ctx.closePath();

        this[sColumnsRanges] = this.columnRanges;

        for (let x = columnWidth / 2, labelIdx = 0; x < this.width - gap, labelIdx < labels.length; x += columnWidth, ++labelIdx) {
            let label = labels[labelIdx];
            this.ctx.font = '10px Lato';
            this.ctx.textAlign = 'center';
            this.ctx.fillStyle = this.activeValue === labelIdx ? Chart.hexRgb('#ffffff') : Chart.hexRgb('#79acb3');
            this.ctx.fillText(label.toUpperCase(), x, this.height - this.options.labelsMarginBottom);
        }
    }


    draw() {
        throw new Error("Method not overrided");
    }

    static hexRgb(color, opacity = 1) {
        let rgb = hexRgb(color);
        return `rgba(${rgb.join(',')}, ${opacity})`;
    }

    static joinPoints(points) {
        let joinedPoints = [];
        points.forEach((pair) => {
            joinedPoints = joinedPoints.concat(pair);
        });

        return joinedPoints;
    }
}