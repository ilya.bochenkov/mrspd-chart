import Chart from './chart';
import { Orientation } from "./chart";

export default class Line extends Chart {
    constructor(element, options, dataset) {
        super(element, options, dataset);
    }

    getLineGradient(opacity = 1) {
        let gradient = this.ctx.createLinearGradient(0, 0, this.width, 0);

        this.options.lineGradient.forEach((point) => {
            gradient.addColorStop(point[0], Chart.hexRgb(point[1], opacity));
        });

        return gradient;
    }

    getLabelLineGradient(y1, y2, opacity = 1) {
        let gradient = this.ctx.createLinearGradient(0, y1, 0, y2);

        this.options.labelLineGradient.forEach((point) => {
            gradient.addColorStop(point[0], Chart.hexRgb(point[1], point[2]));
        });

        return gradient;
    }

    getActiveLabelGradient(y, height) {
        let gradient = this.ctx.createLinearGradient(0, y, 0, height);

        this.options.activeLabelGradient.forEach((point) => {
            gradient.addColorStop(point[0], Chart.hexRgb(point[1], point[2]));
        });

        return gradient;
    }

    draw(showLabels = false) {
        this.ctx.clearRect(0, 0, this.width, this.height);
        this.drawDots();

        if (this.dataset.values.length) {
            this.startUnderLineClip();
            this.drawActiveLabel();
            this.endUnderLineClip();
            this.drawLine();
            if (showLabels) this.drawValues();
            this.drawPoints();
            this.drawLabels();
        }
    }

    clickHandler(e) {
        let columnRanges = this.columnRanges;
        let range = columnRanges.find((range) => {
            return range[0] <= e.offsetX && range[1] > e.offsetX;
        });

        this.activeValue = columnRanges.indexOf(range);
        this.draw(true);

        if (this.options.onClick) {
            this.options.onClick(this.activeValue);
        }
    }

    drawPoints() {
        let points = this.getLinePoints();

        points.forEach((point, index) => {
            this.ctx.beginPath();
            this.ctx.lineWidth = this.options.pointBorder;
            this.ctx.arc(point[0], point[1], this.options.pointDia, 0, 2 * Math.PI, true);
            this.ctx.strokeStyle = this.getLineGradient();
            this.ctx.stroke();
            this.ctx.fillStyle = (index - 1 == this.activeValue) ? this.getLineGradient() : '#0f2a38';
            this.ctx.fill();
            this.ctx.closePath();
        });
    }

    drawLine() {
        let points = Chart.joinPoints(this.getLinePoints());

        this.ctx.beginPath();
        this.ctx.strokeStyle = this.getLineGradient();
        this.ctx.moveTo(points[0], points[1]);
        this.ctx.curve(points);
        this.ctx.lineWidth = 2;
        this.ctx.stroke();
        this.ctx.closePath();
    }

    startUnderLineClip() {
        this.ctx.save();

        let points = this.getLinePoints();
        this.ctx.beginPath();
        this.ctx.curve(Chart.joinPoints(points));
        this.ctx.lineTo(points.slice(-1)[0][0], this.height + 10);
        this.ctx.lineTo(-10, this.height + 10);
        this.ctx.lineTo(points.slice(0, 1)[0][0], points.slice(0, 1)[0][1]);
        this.ctx.strokeStyle = this.getLineGradient(0);
        this.ctx.lineWidth = 2;
        this.ctx.stroke();
        this.ctx.clip();
    }

    endUnderLineClip() {
        this.ctx.restore();
    }

    drawActiveLabel() {
        let points = this.getLinePoints(),
            columnWidth = this.columnWidth,
            x1 = this.activeValue * columnWidth;

        this.ctx.fillStyle = this.getActiveLabelGradient(points[this.activeValue][1], this.height);
        this.ctx.fillRect(x1, 0, columnWidth, this.height);
    }

    drawValues() {
        let points = this.getLinePoints(true),
            point = points[this.activeValue],
            datasetValues = this.dataset.values,
            datasetValue = datasetValues[this.activeValue + 1],
            datasetValuePrev = datasetValues[this.activeValue],
            diff = datasetValue - datasetValuePrev,
            xMargin = 20,
            textSeparatorWidth = 5,
            textHeight = 5,
            x = point[0],
            y = this.options.marginTop - 30;

        if (this.options.orientation === Orientation.UP) {
            diff = -diff;
        }

        this.ctx.font = '15px Lato';
        let leftTextWidth = this.ctx.measureText(datasetValue).width - textSeparatorWidth;

        this.ctx.font = '10px Lato';
        let rightTextWidth = this.ctx.measureText(diff).width + textSeparatorWidth * 3;

        if (diff !== 0) {
            if (x - leftTextWidth < xMargin) {
                x = xMargin + leftTextWidth;
            }

            if (x + rightTextWidth > this.width - xMargin) {
                x = this.width - rightTextWidth;
            }
        } else {
            textSeparatorWidth = 0;
        }

        this.ctx.fillStyle = Chart.hexRgb('#ffffff');
        this.ctx.font = '15px Lato';
        this.ctx.textAlign = diff === 0 ? 'center' : 'right';
        if (this.options.orientation === Orientation.UP) {
            this.ctx.fillText(datasetValue + '%', x - textSeparatorWidth, y);
        } else {
            this.ctx.fillText(datasetValue, x - textSeparatorWidth, y);
        }

        this.ctx.beginPath();
        this.ctx.moveTo(point[0], point[1]);
        this.ctx.lineTo(point[0], y);
        /*this.ctx.moveTo(x, 0);
        this.ctx.lineTo(x, 200);*/
        this.ctx.lineWidth = 1;
        this.ctx.strokeStyle = this.getLabelLineGradient(point[1], y);
        this.ctx.stroke();
        this.ctx.closePath();

        if (diff !== 0) {
            this.ctx.fillStyle = Chart.hexRgb(diff > 0 ? '#e04965' : '#39cfb6');
            this.ctx.strokeStyle = Chart.hexRgb(diff > 0 ? '#e04965' : '#39cfb6');
            this.ctx.font = '10px Lato';
            this.ctx.textAlign = 'left';
            this.ctx.fillText(Math.abs(diff), x + textSeparatorWidth * 2, y - 4);

            this.drawTriangle(x + textSeparatorWidth, y - 11, diff < 0 ? 'top' : 'bottom');
        }
    }

    drawTriangle(x, y, dir = 'bottom') {
        let height = 7,
            width = 3;

        if (dir == 'top') {
            height = -height;
            width = -width;
            y -= height;
        }

        this.ctx.beginPath();
        this.ctx.moveTo(x, y);
        this.ctx.lineTo(x, y + height);
        this.ctx.lineTo(x + width, y + (height - width));
        this.ctx.moveTo(x, y + height);
        this.ctx.lineTo(x - width, y + (height - width));
        this.ctx.stroke();
        this.ctx.closePath();
    }

    getLinePoints(skipFakes = false) {
        let columnWidth = this.columnWidth,
            lineAreaHeight = this.height - this.heightMargins,
            values = this.values.slice(0),
            firstValue = values.slice(0, 1),
            lastValue = values.slice(-1),
            orientation = this.options.orientation;

        if (!skipFakes) {
            values.splice(0, 0, firstValue - firstValue * 0.2);
            values.push(lastValue - lastValue * 0.2);
        }

        return values.map((value, index) => {
            let offset = index - (skipFakes ? 0 : 1);
            if (orientation === Orientation.UP) {
                value = 100 - value;
            }
            return [columnWidth * offset + columnWidth / 2, (lineAreaHeight / 100 * value) + this.options.marginTop];
        });
    }

    get columnWidth() {
        return this.width / this.values.length;
    }
}