import {Line, Orientation} from '../index';

let div = document.createElement('div'),
    canvas = document.createElement('canvas'),
    button = document.createElement('button');

canvas.id = 'chart';
canvas.setAttribute('width', 320);
canvas.setAttribute('height', 200);

div.appendChild(canvas);
div.style.backgroundColor = '#0c2b3d';
div.style.position = 'absolute';
div.style.height = '200px';
div.style.overflow = 'hidden';

button.innerHTML = 'refresh';
button.style.marginBottom = '10px';

document.body.appendChild(button);
document.body.appendChild(div);


let dataset = {
    labels: ['янв', 'фер', 'мар', 'апр', 'май'],
    values: [1195, 1195, 2289, 3340, 1331, 3738]
};

let line = new Line(canvas, {orientation: Orientation.DOWN}, dataset);

function getRnd(min = 1, max = 5000)
{
    return Math.round(Math.random() * (max - min) + min);
}

button.addEventListener('click', () => {
    let newDataset = Object.assign({}, dataset, {values: [getRnd(), getRnd(), getRnd(), getRnd(), getRnd(), getRnd()]});
    line.dataset = newDataset;
});

